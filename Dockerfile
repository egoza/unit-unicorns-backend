FROM openjdk:8-jdk-alpine

LABEL maintainer="petukyeh@fel.cvut.cz"

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/qwerty-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} qwerty.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/qwerty.jar"]