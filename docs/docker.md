#How to run database locally
##Simple run:
1. Install Docker application 

2. Go to backend folder

3. Run:
```
mvn clean install
docker build -t unit-backend .
docker-compose up -d
```
Done.

After you finish, use this to stop all containers:

```
docker-compose down
```

----

This will run PostgreSQL and pgAdmin on youe machine. Every time empty database is created. Your connection to database in pgAdmin will be saved.

-----

##PgAdmin:
To use pgAdmin, go to:
    ```
    http://localhost:5050
    ```
use this login
    ```
    pgadmin4@pgadmin.org
    ```
and pass 
    ```
    admin
    ```
. Now set up default server with this credentials:
```
-host name: postgres
-port: 5432
-maintenance database: postgres
-user name: postgres
-pass: postgres
```
Even after restarting docker, your connection in pgAdmin will be saved

-----

##Advanced usage:
If you don't want to run pgAdmin, you can force docker to run only database with this command:
```
docker-compose -f docker-compose.override.yml up -d
```

------
##Rest endpoints: 
Public DNS (IPv4): `ec2-18-197-109-222.eu-central-1.compute.amazonaws.com:80`

#### Get hints by item names.
Method: Get.
`/hintsByItems`

Request Body format:
`[{"name":"item_name"},
...
]`

#### Get all items.
 method: Get.
`/allItems`

#### Add/Update items.
Method: Post. `/addItems`

Request Body format:

```
[
    {
     "name":"item_name",
     "hints":
        [
            {
             "shortHint":"Short hint text",
             "longHint":"Long hint text"
            }, 
            ...
        ]
    },
    ...
]
```


#### Add/Update item.
Method: Post. `/addItem`

Request Body format:

```
{
    "name":"item_name",
    "hints":
        [
            {
             "shortHint":"Short hint text",
             "longHint":"Long hint text"
            }, 
            ...
        ]
}
```