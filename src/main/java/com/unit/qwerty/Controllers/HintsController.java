package com.unit.qwerty.Controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unit.qwerty.Initialization.InitComponent;
import com.unit.qwerty.Models.Hint;
import com.unit.qwerty.Repositories.HintRepository;
import com.unit.qwerty.Repositories.ItemRepository;
import com.unit.qwerty.Models.Item;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class HintsController {

    private final ItemRepository itemRepository;
    private final HintRepository hintRepository;

    public HintsController(ItemRepository itemRepository, HintRepository hintRepository) {
        this.itemRepository = itemRepository;
        this.hintRepository = hintRepository;
    }

    @PostMapping("/hintsByItems")
    public List<Item> getHintsByItem(@RequestBody List<Item> items){

        return items.stream()
                .map(o-> itemRepository.findByName(o.getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/allItems")
    public List<Item> allItems(){

        List<Item> items =  new ArrayList<>();
        itemRepository.findAll().forEach(items::add);

        return items;
    }

    @PostMapping("/addItems")
    public void addItems(@RequestBody List<Item> items){
        if(items == null) return;

        items.forEach(
                i->{
                    Item item = itemRepository.findByName(i.getName());
                    List<Hint> hints;
                    if(item == null) {
                        item =i;
                        hints = item.getHints();
                    } else {
                        hints = i.getHints();

                        List<Hint> attachedHints = item.getHints();

                        if(hints!=null){
                            hints.addAll(
                                    attachedHints.stream().filter(h->
                                            hints.stream().map(Hint::getShortHint).noneMatch(s-> s.equals(h.getShortHint()))
                                    ).collect(Collectors.toList())
                            );
                        }

                    }

                    if(hints != null) {

                        item.setHints( hints.stream().map(h->{
                            Hint tmp = hintRepository.findByShortHint(h.getShortHint());
                            if(tmp == null){
                                return hintRepository.save(h);
                            }
                            return tmp;
                        }).collect(Collectors.toList()));
                    }
                    itemRepository.save(item);

                }
        );

    }

    @PostMapping("/addItem")
    public void addItem(@RequestBody Item item){
        addItems(Collections.singletonList(item));
    }

    @DeleteMapping("/deleteHint/{hintId}")
    public void deleteHint(@PathVariable int hintId) {
        Hint hint;
        try
        {
            hint = hintRepository.findById(hintId).orElse(null);
        } catch (EmptyResultDataAccessException ex) {
            // it's ok :)
            return;
        }
        if(hint != null){
            hint.getItems().forEach(i -> {
                i.getHints().remove(hint);
                itemRepository.save(i);
            });
            hintRepository.deleteById(hintId);
        }

    }

    @PatchMapping("/updateHint/{hintId}")
    public void updateHint(@PathVariable int hintId,@RequestBody Hint hint) {


        Hint oldHint = hintRepository.findById(hintId).orElse(null);

        if(oldHint==null){
            hintRepository.save(hint);
        } else {
            if (hint.getLongHint() == null) {
                hint.setLongHint(oldHint.getLongHint());
            }

            hintRepository.save(hint);
        }

    }


    @GetMapping("/rescueMe")
    public void rescueMe(){
        itemRepository.deleteAll();

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            InputStream is = getClass().getResourceAsStream("/default_database.json");
            List<Item> items = objectMapper.readValue(is, new TypeReference<List<Item>>() {});
            for (Item item : items) {
                itemRepository.save(item);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
