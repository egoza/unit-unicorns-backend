package com.unit.qwerty.Initialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unit.qwerty.Models.Item;
import com.unit.qwerty.Repositories.HintRepository;
import com.unit.qwerty.Repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

@Component
public class InitComponent {

    private final ItemRepository itemRepository;
    private final HintRepository hintRepository;

    private Logger logger = Logger.getLogger(InitComponent.class.getName());

    @Autowired
    public InitComponent(ItemRepository itemRepository, HintRepository hintRepository) {
        this.itemRepository = itemRepository;
        this.hintRepository = hintRepository;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (itemRepository.count() == 0 || hintRepository.count() == 0) {
            logger.info("Database is in inconsistent state");
            insertDefaultValues();

            logger.info("Default data was inserted");
        }

        logger.info("Database state check passed successfully");
    }

    private void insertDefaultValues() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            InputStream is = getClass().getResourceAsStream("/default_database.json");
            List<Item> items = objectMapper.readValue(is, new TypeReference<List<Item>>() {});
            for (Item item : items) {
                itemRepository.save(item);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
