package com.unit.qwerty.Repositories;

import com.unit.qwerty.Models.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Integer> {

    Item findByName(String name);
}
