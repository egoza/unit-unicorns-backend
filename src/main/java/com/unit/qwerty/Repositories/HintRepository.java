package com.unit.qwerty.Repositories;

import com.unit.qwerty.Models.Hint;
import org.springframework.data.repository.CrudRepository;

public interface HintRepository extends CrudRepository<Hint, Integer> {
    Hint findByShortHint(String shortHint);
}
