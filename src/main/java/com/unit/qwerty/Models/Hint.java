package com.unit.qwerty.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@ToString
public class Hint {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private int id;

    @NonNull
    private String shortHint;

    @NonNull
    private String longHint;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH},mappedBy = "hints")
    @JsonIgnore
    private List<Item> items;

    public Hint(String shortHint, String longHint) {
        this.shortHint = shortHint;
        this.longHint = longHint;
    }
}
