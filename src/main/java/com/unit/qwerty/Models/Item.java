package com.unit.qwerty.Models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@ToString
public class Item {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private int id;

    @NonNull
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REMOVE,CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(
        name = "item_hint",
        joinColumns = @JoinColumn(name = "item_id"),
        inverseJoinColumns = @JoinColumn(name = "hint_id")
    )
    private List<Hint> hints;

    public Item(String name, List<Hint> hints) {
        this.name = name;
        this.hints = hints;
    }
}